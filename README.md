# EdgeNet360

### Semantic Scene Completion from a Single 360 degree Image and Depth Map

![teaser](paper/teaser.png)

If you find this code useful, we kindly resquest to cite our paper below:

* Dourado, A., Kim, H., de Campos, T. and Hilton, A. <br />
  Semantic Scene Completion from a Single 360-Degree Image and Depth Map.<br />
  In *Proceedings of the 15th International Joint Conference on Computer Vision, Imaging and Computer Graphics Theory and Applications (VISIGRAPP 2020) - Volume 5: VISAPP*, pages 36-46  
  [ [pdf](http://www.scitepress.org/DigitalLibrary/Link.aspx?doi=10.5220/0008877700360046) | [bibtex](https://gitlab.com/UnBVision/edgenet360/blob/master/paper/paper.bib) ]

The paper below presents the first use case of this technique as part of a spatial audio reproduction system:
* Kim, H.,  Remaggi, L., Dourado, A, de Campos, T., Jackson, P. and Hilton, A. <br />
  Immersive Audio-Visual Scene Reproduction using Semantic Scene Reconstruction from 360 Cameras <br />
  IEEE Transactions on Circuits and Systems for Video Technology (Submitted)


## Contents
0. [Organization](#organization)
0. [Requirements](#requirements)
0. [Compiling](#compiling)
0. [Inferring over 360 Stereoscopic Images](#Inferringover360stereoscopicimages )
0. [Inferring over Stanford 2D-3D-Semantics Dataset](#inferringoverstanford2d-3d-semanticsdataset )

## Organization 
The project folder is organized as follows:
``` shell
    EdgeNet360
         |-- Data                  //360 stereoscopic images (RGB + depth maps)
         |-- lib_edgenet360        //py-cuda integration and other EdgeNet python libraries
         |-- src                   //cuda source code  (preprocessing)
```

## Requirements 
1. Hardware Requirements (minimum):

   * Ubuntu 16.04 LTS or newer
   * For training: a 11G GPU (for example, a GTX 1080TI) in order to train EdgeNet with a batch-size of 3 scenes
   * For inference only: a 4G GPU (for example, a GTX 1050TI) 

2. Software Requirements:

All software listed here are open-source.
 
   * GPU Drivers
   * Python 3
   * Anaconda 3: We recomend using an [Anaconda](https://www.anaconda.com/distribution/) virtual environment 
     for simplicity. 
   * Tensorflow with GPU/CUDA support. We suggest creating and activating a Anaconda virtual environment for 
     TensorFlow with GPU support using [these instructions](https://www.anaconda.com/tensorflow-in-anaconda/). Use one of the following commands:
``` shell
    /vol/vssp/signsrc/externalLibs/Anaconda/Anaconda3.ncc/bin/conda create -n cuda9  tensorflow-gpu=1.12 #tensorflow 1.12 with cuda 9
    /vol/vssp/signsrc/externalLibs/Anaconda/Anaconda3.ncc/bin/conda create -n cuda10 tensorflow-gpu=1.13 #tensorflow 1.13 with cuda 10
```
   * After creating your environment, activate it, using one of the following commands:
``` shell
    source activate cuda9
    source activate cuda10
```

   * [Keras](https://anaconda.org/conda-forge/keras) .
   * [OpenCV 3 for python](https://anaconda.org/anaconda/py-opencv).
   * Python common libraries [pandas](https://anaconda.org/anaconda/pandas), 
                             [matplotlib](https://anaconda.org/conda-forge/matplotlib),
                             [sklearn](https://anaconda.org/anaconda/scikit-learn),
                             and other common packages according to your environment... 
For example:
``` shell
    conda install -c conda-forge keras
    conda install -c anaconda py-opencv
    conda install -c anaconda pandas
    conda install -c conda-forge matplotlib
    conda install -c anaconda scikit-learn
```
   

## Compiling
To compile the cuda preprocessing shared library: 
``` shell
    cd src
    nvcc --ptxas-options=-v --compiler-options '-fPIC' -o lib_edgenet360.so --shared -std=c++11 lib_edgenet360.cu
```
         
## Inferring over 360 Stereoscopic Images

### Depth map enhancement
Help:
``` shell
    python enhance360.py -h
```
Example:

``` shell
    python enhance360.py DWRC shifted-disparity.png shifted_t.png new_shifted-disparity.png
```
### Prediction
Help:
``` shell
    python infer360.py -h
```
Example:

``` shell
    python infer360.py DWRC new_shifted-disparity.png shifted_t.png DWRC
```

         
         
## Inferring over Stanford 2D-3D-Semantics Dataset


### Download Dataset

Only necessary for evaluation purposes. You can download a subset of the data.
Download dataset following instructions in [2D-3D-Semantics page](http://buildingparser.stanford.edu/dataset.ht).
After download, the data should be organized as follows:
``` shell
    dataset_root
         |-- area_1                
             |-- 3d
             |-- data
             |-- pano 
         |-- area_2                
             |-- 3d
             |-- data
             |-- pano
         ...     
```
### Extract Room Ground Truth

The ground truth extraction is performed on a per area basis.
Find the script read_pointcloud_mat.py and edit the lines below according to your environment: 

``` shell
    area_dir='area_6' #desired area
    base_dir = '/d02/data/stanford' #2D-3D Semantics dataset root
    output_dir = '/d02/data/stanford_processed' #folder to put extracted groung truth
```

Save and run the script:
``` shell
    python read_pointcloud_mat.py
```


### Preprocess Images

To prepare one scene for inference, use the script preproc360_stanford.py. This will generate a Manhattan aligned 360 
panorama and depth map.

Help:  

``` shell
    python preproc360_stanford.py -h
```

Run:  

``` shell
    python preproc360_stanford.py area_3  office_7 0e30c45ea0604ddeb7467fd384362503 --in_path=./Data/stanford/ --out_path=./Data/stanford_processed
```

To prepare all dataset for inference, find the script preproc360_stanford_batch.py, and adjust the lines below, 
according to your environment. 

``` shell
    in_path = '/d02/data/stanford'
    processed_path = '/d02/data/stanford_processed'
```
Save and run the script:
``` shell
    python preproc360_stanford_batch.py
```

### Inference 

Help:  

``` shell
    python infer360_stanford.py -h
```

Run:  

``` shell
    python infer360_stanford.py area_3  office_7 0e30c45ea0604ddeb7467fd384362503 --base_path=./Data/stanford_processed
```

### Evaluation 

To prepare all dataset for inference, find the script eval_stanford.py, and adjust the lines below, 
according to your environment. 

``` shell
    processed_path = '/d02/data/stanford_processed'
```

Save and run the script:
``` shell
    python eval_stanford.py
```


